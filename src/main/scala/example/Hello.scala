package example

import custom.TCustomGreeting
import org.reflections.Reflections

import scala.collection.parallel.CollectionConverters._
import scala.language.reflectiveCalls

object Hello extends Greeting {

  def main(args: Array[String]): Unit = {
    // printing hello
    println(greeting)

    // printing custom greetings
    printingCustomGreetings
  }

  /**
   * https://stackoverflow.com/questions/1469958/scala-how-do-i-dynamically-instantiate-an-object-and-invoke-a-method-using-refl
   * https://stackoverflow.com/questions/520328/can-you-find-all-classes-in-a-package-using-reflection
   */
  def printingCustomGreetings: Unit = {
    // listing names of classes under custom package
    val reflections: Reflections = new Reflections("custom");
    val customGreetingClasses: Array[String] = reflections.getSubTypesOf(classOf[TCustomGreeting]).toArray.map(_.toString.replaceFirst("class ", ""))

    // printing custom greetings
    println("Custom greetings:")
    customGreetingClasses.par.foreach { clazz =>
      val foo = Class.forName(clazz).newInstance.asInstanceOf[ {def printCustomGreetingSentence}]
      foo.printCustomGreetingSentence
    }
  }
}
