package custom

class CustomGreeting_gps extends TCustomGreeting {

  override val firstName: String = "gps"
  val hobbie1: String = "reading"
  val hobbie2: String = "sports"

  override def customGreetingSentence: String = s"$greeting, my name is $firstName. My Hobbies are $hobbie1 and $hobbie2."

}
