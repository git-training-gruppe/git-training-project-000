package custom

class CustomGreeting_cgomis_jvkrauth  extends TCustomGreeting {


  override val firstName: String = "Jean-Victor"
  val hobbie1: String = "metal"
  val hobbie2: String = "swimming"

  override  def customGreetingSentence : String = s"Hello, My name is $firstName. I like $hobbie1 and $hobbie2."
}
