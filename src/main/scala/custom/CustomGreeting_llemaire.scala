package custom

class CustomGreeting_llemaire extends TCustomGreeting {
  val firstName: String = "Lucas"
  val hobbie1: String = "Speedrun"
  val hobbie2: String = "Rock"

  override def customGreetingSentence: String = s"Salut, $firstName. Tes hobbies sont : $hobbie1 et $hobbie2"
}
