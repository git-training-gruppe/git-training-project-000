package custom

class CustomGreeting_vrogissart extends TCustomGreeting {
  val firstName :String = "Vincent"
  val hobbie1: String = "videogames"
  val hobbie2: String = "sports"

  override def customGreetingSentence: String = s"$greeting, my name is $firstName. My Hobbies are $hobbie1 and $hobbie2."
}
