package custom

import example.Greeting

trait TCustomGreeting extends Greeting {

  val firstName: String
  val hobbie1: String = "reading"
  val hobbie2: String = "sports"

  def customGreetingSentence: String = s"$greeting, my name is $firstName. My Hobbies are reading and sports."

  def printCustomGreetingSentence: Unit = println(customGreetingSentence)
}
