package custom

class CustomGreeting_cgomis_jmenudier extends TCustomGreeting {

  val firstName: String = "Jeoffrey"

  val hobbie1: String = "running"
  val hobbie2: String = "watching series"
  override def customGreetingSentence: String = s"$greeting, my name is $firstName. My Hobbies are $hobbie1 and $hobbie2"
}
