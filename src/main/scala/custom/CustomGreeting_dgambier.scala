package custom

class CustomGreeting_dgambier extends TCustomGreeting {
  val firstName: String = "Delphine"
  val hobbie1: String = "reading"
  val hobbie2: String = "sports"

  override def customGreetingSentence: String = s"$greeting, my name is $firstName. My Hobbies are $hobbie1 and $hobbie2.Nice to meet you."
}
