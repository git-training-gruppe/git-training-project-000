package custom

class CustomGreeting_cgomis_pmillet extends TCustomGreeting {

  val firstName : String = "Pierre"

  override def customGreetingSentence: String = s"Hey $firstName, welcome to this ---conflict--- project."
}
