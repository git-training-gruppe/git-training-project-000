package custom

class CustomGreeting_jpalluel extends TCustomGreeting {
  val firstName: String = "Delphine"

  override def customGreetingSentence: String = s"$greeting, my name is $firstName. Nice to meet you. Your Hobbies are $hobbie1 and $hobbie2 !"

}
