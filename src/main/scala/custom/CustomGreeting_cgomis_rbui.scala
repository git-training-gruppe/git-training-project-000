package custom

import custom.TCustomGreeting

class _MetasilveurTCustomGreeting extends TCustomGreeting {

  val firstName: String = "Robert"
  val hobbie1: String = "reading"
  val hobbie2: String = "sports"

  override def customGreetingSentence: String = s"$greeting, my name is $firstName. My hobbies are $hobbie1 and $hobbie2"

}
