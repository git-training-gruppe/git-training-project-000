import sbt._

object Dependencies {

  lazy val testDeps = Seq(
    // https://mvnrepository.com/artifact/org.scalatest/scalatest
    "org.scalatest" %% "scalatest" % "3.2.12"
  )

  lazy val providedDeps = Seq(

  )

  lazy val deps = Seq(
    // https://mvnrepository.com/artifact/org.reflections/reflections
    "org.reflections" % "reflections" % "0.10.2",

    // https://mvnrepository.com/artifact/org.scala-lang.modules/scala-parallel-collections
    "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
  )
}
